{ fetchurl, fetchgit, linkFarm, runCommandNoCC, gnutar }: rec {
  offline_cache = linkFarm "offline" packages;
  packages = [
    {
      name = "_fullhuman_postcss_purgecss___postcss_purgecss_3.1.3.tgz";
      path = fetchurl {
        name = "_fullhuman_postcss_purgecss___postcss_purgecss_3.1.3.tgz";
        url  = "https://registry.yarnpkg.com/@fullhuman/postcss-purgecss/-/postcss-purgecss-3.1.3.tgz";
        sha1 = "47af7b87c9bfb3de4bc94a38f875b928fffdf339";
      };
    }
    {
      name = "_nodelib_fs.scandir___fs.scandir_2.1.5.tgz";
      path = fetchurl {
        name = "_nodelib_fs.scandir___fs.scandir_2.1.5.tgz";
        url  = "https://registry.yarnpkg.com/@nodelib/fs.scandir/-/fs.scandir-2.1.5.tgz";
        sha1 = "7619c2eb21b25483f6d167548b4cfd5a7488c3d5";
      };
    }
    {
      name = "_nodelib_fs.stat___fs.stat_2.0.5.tgz";
      path = fetchurl {
        name = "_nodelib_fs.stat___fs.stat_2.0.5.tgz";
        url  = "https://registry.yarnpkg.com/@nodelib/fs.stat/-/fs.stat-2.0.5.tgz";
        sha1 = "5bd262af94e9d25bd1e71b05deed44876a222e8b";
      };
    }
    {
      name = "_nodelib_fs.walk___fs.walk_1.2.8.tgz";
      path = fetchurl {
        name = "_nodelib_fs.walk___fs.walk_1.2.8.tgz";
        url  = "https://registry.yarnpkg.com/@nodelib/fs.walk/-/fs.walk-1.2.8.tgz";
        sha1 = "e95737e8bb6746ddedf69c556953494f196fe69a";
      };
    }
    {
      name = "_sindresorhus_is___is_0.14.0.tgz";
      path = fetchurl {
        name = "_sindresorhus_is___is_0.14.0.tgz";
        url  = "https://registry.yarnpkg.com/@sindresorhus/is/-/is-0.14.0.tgz";
        sha1 = "9fb3a3cf3132328151f353de4632e01e52102bea";
      };
    }
    {
      name = "_szmarczak_http_timer___http_timer_1.1.2.tgz";
      path = fetchurl {
        name = "_szmarczak_http_timer___http_timer_1.1.2.tgz";
        url  = "https://registry.yarnpkg.com/@szmarczak/http-timer/-/http-timer-1.1.2.tgz";
        sha1 = "b1665e2c461a2cd92f4c1bbf50d5454de0d4b421";
      };
    }
    {
      name = "abbrev___abbrev_1.1.1.tgz";
      path = fetchurl {
        name = "abbrev___abbrev_1.1.1.tgz";
        url  = "https://registry.yarnpkg.com/abbrev/-/abbrev-1.1.1.tgz";
        sha1 = "f8f2c887ad10bf67f634f005b6987fed3179aac8";
      };
    }
    {
      name = "acorn_node___acorn_node_1.8.2.tgz";
      path = fetchurl {
        name = "acorn_node___acorn_node_1.8.2.tgz";
        url  = "https://registry.yarnpkg.com/acorn-node/-/acorn-node-1.8.2.tgz";
        sha1 = "114c95d64539e53dede23de8b9d96df7c7ae2af8";
      };
    }
    {
      name = "acorn_walk___acorn_walk_7.2.0.tgz";
      path = fetchurl {
        name = "acorn_walk___acorn_walk_7.2.0.tgz";
        url  = "https://registry.yarnpkg.com/acorn-walk/-/acorn-walk-7.2.0.tgz";
        sha1 = "0de889a601203909b0fbe07b8938dc21d2e967bc";
      };
    }
    {
      name = "acorn___acorn_7.4.1.tgz";
      path = fetchurl {
        name = "acorn___acorn_7.4.1.tgz";
        url  = "https://registry.yarnpkg.com/acorn/-/acorn-7.4.1.tgz";
        sha1 = "feaed255973d2e77555b83dbc08851a6c63520fa";
      };
    }
    {
      name = "ansi_align___ansi_align_3.0.0.tgz";
      path = fetchurl {
        name = "ansi_align___ansi_align_3.0.0.tgz";
        url  = "https://registry.yarnpkg.com/ansi-align/-/ansi-align-3.0.0.tgz";
        sha1 = "b536b371cf687caaef236c18d3e21fe3797467cb";
      };
    }
    {
      name = "ansi_regex___ansi_regex_4.1.0.tgz";
      path = fetchurl {
        name = "ansi_regex___ansi_regex_4.1.0.tgz";
        url  = "https://registry.yarnpkg.com/ansi-regex/-/ansi-regex-4.1.0.tgz";
        sha1 = "8b9f8f08cf1acb843756a839ca8c7e3168c51997";
      };
    }
    {
      name = "ansi_regex___ansi_regex_5.0.1.tgz";
      path = fetchurl {
        name = "ansi_regex___ansi_regex_5.0.1.tgz";
        url  = "https://registry.yarnpkg.com/ansi-regex/-/ansi-regex-5.0.1.tgz";
        sha1 = "082cb2c89c9fe8659a311a53bd6a4dc5301db304";
      };
    }
    {
      name = "ansi_styles___ansi_styles_3.2.1.tgz";
      path = fetchurl {
        name = "ansi_styles___ansi_styles_3.2.1.tgz";
        url  = "https://registry.yarnpkg.com/ansi-styles/-/ansi-styles-3.2.1.tgz";
        sha1 = "41fbb20243e50b12be0f04b8dedbf07520ce841d";
      };
    }
    {
      name = "ansi_styles___ansi_styles_4.3.0.tgz";
      path = fetchurl {
        name = "ansi_styles___ansi_styles_4.3.0.tgz";
        url  = "https://registry.yarnpkg.com/ansi-styles/-/ansi-styles-4.3.0.tgz";
        sha1 = "edd803628ae71c04c85ae7a0906edad34b648937";
      };
    }
    {
      name = "anymatch___anymatch_3.1.2.tgz";
      path = fetchurl {
        name = "anymatch___anymatch_3.1.2.tgz";
        url  = "https://registry.yarnpkg.com/anymatch/-/anymatch-3.1.2.tgz";
        sha1 = "c0557c096af32f106198f4f4e2a383537e378716";
      };
    }
    {
      name = "argparse___argparse_1.0.10.tgz";
      path = fetchurl {
        name = "argparse___argparse_1.0.10.tgz";
        url  = "https://registry.yarnpkg.com/argparse/-/argparse-1.0.10.tgz";
        sha1 = "bcd6791ea5ae09725e17e5ad988134cd40b3d911";
      };
    }
    {
      name = "at_least_node___at_least_node_1.0.0.tgz";
      path = fetchurl {
        name = "at_least_node___at_least_node_1.0.0.tgz";
        url  = "https://registry.yarnpkg.com/at-least-node/-/at-least-node-1.0.0.tgz";
        sha1 = "602cd4b46e844ad4effc92a8011a3c46e0238dc2";
      };
    }
    {
      name = "autoprefixer___autoprefixer_10.2.5.tgz";
      path = fetchurl {
        name = "autoprefixer___autoprefixer_10.2.5.tgz";
        url  = "https://registry.yarnpkg.com/autoprefixer/-/autoprefixer-10.2.5.tgz";
        sha1 = "096a0337dbc96c0873526d7fef5de4428d05382d";
      };
    }
    {
      name = "balanced_match___balanced_match_1.0.2.tgz";
      path = fetchurl {
        name = "balanced_match___balanced_match_1.0.2.tgz";
        url  = "https://registry.yarnpkg.com/balanced-match/-/balanced-match-1.0.2.tgz";
        sha1 = "e83e3a7e3f300b34cb9d87f615fa0cbf357690ee";
      };
    }
    {
      name = "binary_extensions___binary_extensions_2.2.0.tgz";
      path = fetchurl {
        name = "binary_extensions___binary_extensions_2.2.0.tgz";
        url  = "https://registry.yarnpkg.com/binary-extensions/-/binary-extensions-2.2.0.tgz";
        sha1 = "75f502eeaf9ffde42fc98829645be4ea76bd9e2d";
      };
    }
    {
      name = "bluebird___bluebird_3.7.2.tgz";
      path = fetchurl {
        name = "bluebird___bluebird_3.7.2.tgz";
        url  = "https://registry.yarnpkg.com/bluebird/-/bluebird-3.7.2.tgz";
        sha1 = "9f229c15be272454ffa973ace0dbee79a1b0c36f";
      };
    }
    {
      name = "boolbase___boolbase_1.0.0.tgz";
      path = fetchurl {
        name = "boolbase___boolbase_1.0.0.tgz";
        url  = "https://registry.yarnpkg.com/boolbase/-/boolbase-1.0.0.tgz";
        sha1 = "68dff5fbe60c51eb37725ea9e3ed310dcc1e776e";
      };
    }
    {
      name = "boxen___boxen_4.2.0.tgz";
      path = fetchurl {
        name = "boxen___boxen_4.2.0.tgz";
        url  = "https://registry.yarnpkg.com/boxen/-/boxen-4.2.0.tgz";
        sha1 = "e411b62357d6d6d36587c8ac3d5d974daa070e64";
      };
    }
    {
      name = "brace_expansion___brace_expansion_1.1.11.tgz";
      path = fetchurl {
        name = "brace_expansion___brace_expansion_1.1.11.tgz";
        url  = "https://registry.yarnpkg.com/brace-expansion/-/brace-expansion-1.1.11.tgz";
        sha1 = "3c7fcbf529d87226f3d2f52b966ff5271eb441dd";
      };
    }
    {
      name = "braces___braces_3.0.2.tgz";
      path = fetchurl {
        name = "braces___braces_3.0.2.tgz";
        url  = "https://registry.yarnpkg.com/braces/-/braces-3.0.2.tgz";
        sha1 = "3454e1a462ee8d599e236df336cd9ea4f8afe107";
      };
    }
    {
      name = "browser_stdout___browser_stdout_1.3.1.tgz";
      path = fetchurl {
        name = "browser_stdout___browser_stdout_1.3.1.tgz";
        url  = "https://registry.yarnpkg.com/browser-stdout/-/browser-stdout-1.3.1.tgz";
        sha1 = "baa559ee14ced73452229bad7326467c61fabd60";
      };
    }
    {
      name = "browserslist___browserslist_4.17.1.tgz";
      path = fetchurl {
        name = "browserslist___browserslist_4.17.1.tgz";
        url  = "https://registry.yarnpkg.com/browserslist/-/browserslist-4.17.1.tgz";
        sha1 = "a98d104f54af441290b7d592626dd541fa642eb9";
      };
    }
    {
      name = "bytes___bytes_3.1.0.tgz";
      path = fetchurl {
        name = "bytes___bytes_3.1.0.tgz";
        url  = "https://registry.yarnpkg.com/bytes/-/bytes-3.1.0.tgz";
        sha1 = "f6cf7933a360e0588fa9fde85651cdc7f805d1f6";
      };
    }
    {
      name = "cacheable_request___cacheable_request_6.1.0.tgz";
      path = fetchurl {
        name = "cacheable_request___cacheable_request_6.1.0.tgz";
        url  = "https://registry.yarnpkg.com/cacheable-request/-/cacheable-request-6.1.0.tgz";
        sha1 = "20ffb8bd162ba4be11e9567d823db651052ca912";
      };
    }
    {
      name = "camelcase_css___camelcase_css_2.0.1.tgz";
      path = fetchurl {
        name = "camelcase_css___camelcase_css_2.0.1.tgz";
        url  = "https://registry.yarnpkg.com/camelcase-css/-/camelcase-css-2.0.1.tgz";
        sha1 = "ee978f6947914cc30c6b44741b6ed1df7f043fd5";
      };
    }
    {
      name = "camelcase___camelcase_5.3.1.tgz";
      path = fetchurl {
        name = "camelcase___camelcase_5.3.1.tgz";
        url  = "https://registry.yarnpkg.com/camelcase/-/camelcase-5.3.1.tgz";
        sha1 = "e3c9b31569e106811df242f715725a1f4c494320";
      };
    }
    {
      name = "caniuse_lite___caniuse_lite_1.0.30001260.tgz";
      path = fetchurl {
        name = "caniuse_lite___caniuse_lite_1.0.30001260.tgz";
        url  = "https://registry.yarnpkg.com/caniuse-lite/-/caniuse-lite-1.0.30001260.tgz";
        sha1 = "e3be3f34ddad735ca4a2736fa9e768ef34316270";
      };
    }
    {
      name = "chalk___chalk_2.4.2.tgz";
      path = fetchurl {
        name = "chalk___chalk_2.4.2.tgz";
        url  = "https://registry.yarnpkg.com/chalk/-/chalk-2.4.2.tgz";
        sha1 = "cd42541677a54333cf541a49108c1432b44c9424";
      };
    }
    {
      name = "chalk___chalk_3.0.0.tgz";
      path = fetchurl {
        name = "chalk___chalk_3.0.0.tgz";
        url  = "https://registry.yarnpkg.com/chalk/-/chalk-3.0.0.tgz";
        sha1 = "3f73c2bf526591f574cc492c51e2456349f844e4";
      };
    }
    {
      name = "chalk___chalk_4.1.2.tgz";
      path = fetchurl {
        name = "chalk___chalk_4.1.2.tgz";
        url  = "https://registry.yarnpkg.com/chalk/-/chalk-4.1.2.tgz";
        sha1 = "aac4e2b7734a740867aeb16bf02aad556a1e7a01";
      };
    }
    {
      name = "cheerio___cheerio_0.22.0.tgz";
      path = fetchurl {
        name = "cheerio___cheerio_0.22.0.tgz";
        url  = "https://registry.yarnpkg.com/cheerio/-/cheerio-0.22.0.tgz";
        sha1 = "a9baa860a3f9b595a6b81b1a86873121ed3a269e";
      };
    }
    {
      name = "chokidar___chokidar_3.5.2.tgz";
      path = fetchurl {
        name = "chokidar___chokidar_3.5.2.tgz";
        url  = "https://registry.yarnpkg.com/chokidar/-/chokidar-3.5.2.tgz";
        sha1 = "dba3976fcadb016f66fd365021d91600d01c1e75";
      };
    }
    {
      name = "ci_info___ci_info_2.0.0.tgz";
      path = fetchurl {
        name = "ci_info___ci_info_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/ci-info/-/ci-info-2.0.0.tgz";
        sha1 = "67a9e964be31a51e15e5010d58e6f12834002f46";
      };
    }
    {
      name = "cli_boxes___cli_boxes_2.2.1.tgz";
      path = fetchurl {
        name = "cli_boxes___cli_boxes_2.2.1.tgz";
        url  = "https://registry.yarnpkg.com/cli-boxes/-/cli-boxes-2.2.1.tgz";
        sha1 = "ddd5035d25094fce220e9cab40a45840a440318f";
      };
    }
    {
      name = "clone_response___clone_response_1.0.2.tgz";
      path = fetchurl {
        name = "clone_response___clone_response_1.0.2.tgz";
        url  = "https://registry.yarnpkg.com/clone-response/-/clone-response-1.0.2.tgz";
        sha1 = "d1dc973920314df67fbeb94223b4ee350239e96b";
      };
    }
    {
      name = "color_convert___color_convert_1.9.3.tgz";
      path = fetchurl {
        name = "color_convert___color_convert_1.9.3.tgz";
        url  = "https://registry.yarnpkg.com/color-convert/-/color-convert-1.9.3.tgz";
        sha1 = "bb71850690e1f136567de629d2d5471deda4c1e8";
      };
    }
    {
      name = "color_convert___color_convert_2.0.1.tgz";
      path = fetchurl {
        name = "color_convert___color_convert_2.0.1.tgz";
        url  = "https://registry.yarnpkg.com/color-convert/-/color-convert-2.0.1.tgz";
        sha1 = "72d3a68d598c9bdb3af2ad1e84f21d896abd4de3";
      };
    }
    {
      name = "color_name___color_name_1.1.3.tgz";
      path = fetchurl {
        name = "color_name___color_name_1.1.3.tgz";
        url  = "https://registry.yarnpkg.com/color-name/-/color-name-1.1.3.tgz";
        sha1 = "a7d0558bd89c42f795dd42328f740831ca53bc25";
      };
    }
    {
      name = "color_name___color_name_1.1.4.tgz";
      path = fetchurl {
        name = "color_name___color_name_1.1.4.tgz";
        url  = "https://registry.yarnpkg.com/color-name/-/color-name-1.1.4.tgz";
        sha1 = "c2a09a87acbde69543de6f63fa3995c826c536a2";
      };
    }
    {
      name = "color_string___color_string_1.6.0.tgz";
      path = fetchurl {
        name = "color_string___color_string_1.6.0.tgz";
        url  = "https://registry.yarnpkg.com/color-string/-/color-string-1.6.0.tgz";
        sha1 = "c3915f61fe267672cb7e1e064c9d692219f6c312";
      };
    }
    {
      name = "color___color_3.2.1.tgz";
      path = fetchurl {
        name = "color___color_3.2.1.tgz";
        url  = "https://registry.yarnpkg.com/color/-/color-3.2.1.tgz";
        sha1 = "3544dc198caf4490c3ecc9a790b54fe9ff45e164";
      };
    }
    {
      name = "colorette___colorette_1.4.0.tgz";
      path = fetchurl {
        name = "colorette___colorette_1.4.0.tgz";
        url  = "https://registry.yarnpkg.com/colorette/-/colorette-1.4.0.tgz";
        sha1 = "5190fbb87276259a86ad700bff2c6d6faa3fca40";
      };
    }
    {
      name = "commander___commander_2.15.1.tgz";
      path = fetchurl {
        name = "commander___commander_2.15.1.tgz";
        url  = "https://registry.yarnpkg.com/commander/-/commander-2.15.1.tgz";
        sha1 = "df46e867d0fc2aec66a34662b406a9ccafff5b0f";
      };
    }
    {
      name = "commander___commander_6.2.1.tgz";
      path = fetchurl {
        name = "commander___commander_6.2.1.tgz";
        url  = "https://registry.yarnpkg.com/commander/-/commander-6.2.1.tgz";
        sha1 = "0792eb682dfbc325999bb2b84fddddba110ac73c";
      };
    }
    {
      name = "concat_map___concat_map_0.0.1.tgz";
      path = fetchurl {
        name = "concat_map___concat_map_0.0.1.tgz";
        url  = "https://registry.yarnpkg.com/concat-map/-/concat-map-0.0.1.tgz";
        sha1 = "d8a96bd77fd68df7793a73036a3ba0d5405d477b";
      };
    }
    {
      name = "configstore___configstore_5.0.1.tgz";
      path = fetchurl {
        name = "configstore___configstore_5.0.1.tgz";
        url  = "https://registry.yarnpkg.com/configstore/-/configstore-5.0.1.tgz";
        sha1 = "d365021b5df4b98cdd187d6a3b0e3f6a7cc5ed96";
      };
    }
    {
      name = "crypto_random_string___crypto_random_string_2.0.0.tgz";
      path = fetchurl {
        name = "crypto_random_string___crypto_random_string_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/crypto-random-string/-/crypto-random-string-2.0.0.tgz";
        sha1 = "ef2a7a966ec11083388369baa02ebead229b30d5";
      };
    }
    {
      name = "css_select___css_select_1.2.0.tgz";
      path = fetchurl {
        name = "css_select___css_select_1.2.0.tgz";
        url  = "https://registry.yarnpkg.com/css-select/-/css-select-1.2.0.tgz";
        sha1 = "2b3a110539c5355f1cd8d314623e870b121ec858";
      };
    }
    {
      name = "css_unit_converter___css_unit_converter_1.1.2.tgz";
      path = fetchurl {
        name = "css_unit_converter___css_unit_converter_1.1.2.tgz";
        url  = "https://registry.yarnpkg.com/css-unit-converter/-/css-unit-converter-1.1.2.tgz";
        sha1 = "4c77f5a1954e6dbff60695ecb214e3270436ab21";
      };
    }
    {
      name = "css_what___css_what_2.1.3.tgz";
      path = fetchurl {
        name = "css_what___css_what_2.1.3.tgz";
        url  = "https://registry.yarnpkg.com/css-what/-/css-what-2.1.3.tgz";
        sha1 = "a6d7604573365fe74686c3f311c56513d88285f2";
      };
    }
    {
      name = "cssesc___cssesc_3.0.0.tgz";
      path = fetchurl {
        name = "cssesc___cssesc_3.0.0.tgz";
        url  = "https://registry.yarnpkg.com/cssesc/-/cssesc-3.0.0.tgz";
        sha1 = "37741919903b868565e1c09ea747445cd18983ee";
      };
    }
    {
      name = "debug___debug_3.1.0.tgz";
      path = fetchurl {
        name = "debug___debug_3.1.0.tgz";
        url  = "https://registry.yarnpkg.com/debug/-/debug-3.1.0.tgz";
        sha1 = "5bb5a0672628b64149566ba16819e61518c67261";
      };
    }
    {
      name = "debug___debug_2.6.9.tgz";
      path = fetchurl {
        name = "debug___debug_2.6.9.tgz";
        url  = "https://registry.yarnpkg.com/debug/-/debug-2.6.9.tgz";
        sha1 = "5d128515df134ff327e90a4c93f4e077a536341f";
      };
    }
    {
      name = "debug___debug_3.2.7.tgz";
      path = fetchurl {
        name = "debug___debug_3.2.7.tgz";
        url  = "https://registry.yarnpkg.com/debug/-/debug-3.2.7.tgz";
        sha1 = "72580b7e9145fb39b6676f9c5e5fb100b934179a";
      };
    }
    {
      name = "decompress_response___decompress_response_3.3.0.tgz";
      path = fetchurl {
        name = "decompress_response___decompress_response_3.3.0.tgz";
        url  = "https://registry.yarnpkg.com/decompress-response/-/decompress-response-3.3.0.tgz";
        sha1 = "80a4dd323748384bfa248083622aedec982adff3";
      };
    }
    {
      name = "deep_extend___deep_extend_0.6.0.tgz";
      path = fetchurl {
        name = "deep_extend___deep_extend_0.6.0.tgz";
        url  = "https://registry.yarnpkg.com/deep-extend/-/deep-extend-0.6.0.tgz";
        sha1 = "c4fa7c95404a17a9c3e8ca7e1537312b736330ac";
      };
    }
    {
      name = "defer_to_connect___defer_to_connect_1.1.3.tgz";
      path = fetchurl {
        name = "defer_to_connect___defer_to_connect_1.1.3.tgz";
        url  = "https://registry.yarnpkg.com/defer-to-connect/-/defer-to-connect-1.1.3.tgz";
        sha1 = "331ae050c08dcf789f8c83a7b81f0ed94f4ac591";
      };
    }
    {
      name = "defined___defined_1.0.0.tgz";
      path = fetchurl {
        name = "defined___defined_1.0.0.tgz";
        url  = "https://registry.yarnpkg.com/defined/-/defined-1.0.0.tgz";
        sha1 = "c98d9bcef75674188e110969151199e39b1fa693";
      };
    }
    {
      name = "detective___detective_5.2.0.tgz";
      path = fetchurl {
        name = "detective___detective_5.2.0.tgz";
        url  = "https://registry.yarnpkg.com/detective/-/detective-5.2.0.tgz";
        sha1 = "feb2a77e85b904ecdea459ad897cc90a99bd2a7b";
      };
    }
    {
      name = "didyoumean___didyoumean_1.2.2.tgz";
      path = fetchurl {
        name = "didyoumean___didyoumean_1.2.2.tgz";
        url  = "https://registry.yarnpkg.com/didyoumean/-/didyoumean-1.2.2.tgz";
        sha1 = "989346ffe9e839b4555ecf5666edea0d3e8ad037";
      };
    }
    {
      name = "diff___diff_3.5.0.tgz";
      path = fetchurl {
        name = "diff___diff_3.5.0.tgz";
        url  = "https://registry.yarnpkg.com/diff/-/diff-3.5.0.tgz";
        sha1 = "800c0dd1e0a8bfbc95835c202ad220fe317e5a12";
      };
    }
    {
      name = "dlv___dlv_1.1.3.tgz";
      path = fetchurl {
        name = "dlv___dlv_1.1.3.tgz";
        url  = "https://registry.yarnpkg.com/dlv/-/dlv-1.1.3.tgz";
        sha1 = "5c198a8a11453596e751494d49874bc7732f2e79";
      };
    }
    {
      name = "dom_serializer___dom_serializer_0.2.2.tgz";
      path = fetchurl {
        name = "dom_serializer___dom_serializer_0.2.2.tgz";
        url  = "https://registry.yarnpkg.com/dom-serializer/-/dom-serializer-0.2.2.tgz";
        sha1 = "1afb81f533717175d478655debc5e332d9f9bb51";
      };
    }
    {
      name = "dom_serializer___dom_serializer_0.1.1.tgz";
      path = fetchurl {
        name = "dom_serializer___dom_serializer_0.1.1.tgz";
        url  = "https://registry.yarnpkg.com/dom-serializer/-/dom-serializer-0.1.1.tgz";
        sha1 = "1ec4059e284babed36eec2941d4a970a189ce7c0";
      };
    }
    {
      name = "domelementtype___domelementtype_1.3.1.tgz";
      path = fetchurl {
        name = "domelementtype___domelementtype_1.3.1.tgz";
        url  = "https://registry.yarnpkg.com/domelementtype/-/domelementtype-1.3.1.tgz";
        sha1 = "d048c44b37b0d10a7f2a3d5fee3f4333d790481f";
      };
    }
    {
      name = "domelementtype___domelementtype_2.2.0.tgz";
      path = fetchurl {
        name = "domelementtype___domelementtype_2.2.0.tgz";
        url  = "https://registry.yarnpkg.com/domelementtype/-/domelementtype-2.2.0.tgz";
        sha1 = "9a0b6c2782ed6a1c7323d42267183df9bd8b1d57";
      };
    }
    {
      name = "domhandler___domhandler_2.4.2.tgz";
      path = fetchurl {
        name = "domhandler___domhandler_2.4.2.tgz";
        url  = "https://registry.yarnpkg.com/domhandler/-/domhandler-2.4.2.tgz";
        sha1 = "8805097e933d65e85546f726d60f5eb88b44f803";
      };
    }
    {
      name = "domutils___domutils_1.5.1.tgz";
      path = fetchurl {
        name = "domutils___domutils_1.5.1.tgz";
        url  = "https://registry.yarnpkg.com/domutils/-/domutils-1.5.1.tgz";
        sha1 = "dcd8488a26f563d61079e48c9f7b7e32373682cf";
      };
    }
    {
      name = "domutils___domutils_1.7.0.tgz";
      path = fetchurl {
        name = "domutils___domutils_1.7.0.tgz";
        url  = "https://registry.yarnpkg.com/domutils/-/domutils-1.7.0.tgz";
        sha1 = "56ea341e834e06e6748af7a1cb25da67ea9f8c2a";
      };
    }
    {
      name = "dot_prop___dot_prop_5.3.0.tgz";
      path = fetchurl {
        name = "dot_prop___dot_prop_5.3.0.tgz";
        url  = "https://registry.yarnpkg.com/dot-prop/-/dot-prop-5.3.0.tgz";
        sha1 = "90ccce708cd9cd82cc4dc8c3ddd9abdd55b20e88";
      };
    }
    {
      name = "duplexer3___duplexer3_0.1.4.tgz";
      path = fetchurl {
        name = "duplexer3___duplexer3_0.1.4.tgz";
        url  = "https://registry.yarnpkg.com/duplexer3/-/duplexer3-0.1.4.tgz";
        sha1 = "ee01dd1cac0ed3cbc7fdbea37dc0a8f1ce002ce2";
      };
    }
    {
      name = "electron_to_chromium___electron_to_chromium_1.3.848.tgz";
      path = fetchurl {
        name = "electron_to_chromium___electron_to_chromium_1.3.848.tgz";
        url  = "https://registry.yarnpkg.com/electron-to-chromium/-/electron-to-chromium-1.3.848.tgz";
        sha1 = "94cc196e496f33c0d71cd98561448f10018584cc";
      };
    }
    {
      name = "emoji_regex___emoji_regex_7.0.3.tgz";
      path = fetchurl {
        name = "emoji_regex___emoji_regex_7.0.3.tgz";
        url  = "https://registry.yarnpkg.com/emoji-regex/-/emoji-regex-7.0.3.tgz";
        sha1 = "933a04052860c85e83c122479c4748a8e4c72156";
      };
    }
    {
      name = "emoji_regex___emoji_regex_8.0.0.tgz";
      path = fetchurl {
        name = "emoji_regex___emoji_regex_8.0.0.tgz";
        url  = "https://registry.yarnpkg.com/emoji-regex/-/emoji-regex-8.0.0.tgz";
        sha1 = "e818fd69ce5ccfcb404594f842963bf53164cc37";
      };
    }
    {
      name = "end_of_stream___end_of_stream_1.4.4.tgz";
      path = fetchurl {
        name = "end_of_stream___end_of_stream_1.4.4.tgz";
        url  = "https://registry.yarnpkg.com/end-of-stream/-/end-of-stream-1.4.4.tgz";
        sha1 = "5ae64a5f45057baf3626ec14da0ca5e4b2431eb0";
      };
    }
    {
      name = "entities___entities_1.1.2.tgz";
      path = fetchurl {
        name = "entities___entities_1.1.2.tgz";
        url  = "https://registry.yarnpkg.com/entities/-/entities-1.1.2.tgz";
        sha1 = "bdfa735299664dfafd34529ed4f8522a275fea56";
      };
    }
    {
      name = "entities___entities_2.2.0.tgz";
      path = fetchurl {
        name = "entities___entities_2.2.0.tgz";
        url  = "https://registry.yarnpkg.com/entities/-/entities-2.2.0.tgz";
        sha1 = "098dc90ebb83d8dffa089d55256b351d34c4da55";
      };
    }
    {
      name = "escalade___escalade_3.1.1.tgz";
      path = fetchurl {
        name = "escalade___escalade_3.1.1.tgz";
        url  = "https://registry.yarnpkg.com/escalade/-/escalade-3.1.1.tgz";
        sha1 = "d8cfdc7000965c5a0174b4a82eaa5c0552742e40";
      };
    }
    {
      name = "escape_goat___escape_goat_2.1.1.tgz";
      path = fetchurl {
        name = "escape_goat___escape_goat_2.1.1.tgz";
        url  = "https://registry.yarnpkg.com/escape-goat/-/escape-goat-2.1.1.tgz";
        sha1 = "1b2dc77003676c457ec760b2dc68edb648188675";
      };
    }
    {
      name = "escape_string_regexp___escape_string_regexp_1.0.5.tgz";
      path = fetchurl {
        name = "escape_string_regexp___escape_string_regexp_1.0.5.tgz";
        url  = "https://registry.yarnpkg.com/escape-string-regexp/-/escape-string-regexp-1.0.5.tgz";
        sha1 = "1b61c0562190a8dff6ae3bb2cf0200ca130b86d4";
      };
    }
    {
      name = "esprima___esprima_4.0.1.tgz";
      path = fetchurl {
        name = "esprima___esprima_4.0.1.tgz";
        url  = "https://registry.yarnpkg.com/esprima/-/esprima-4.0.1.tgz";
        sha1 = "13b04cdb3e6c5d19df91ab6987a8695619b0aa71";
      };
    }
    {
      name = "fast_glob___fast_glob_3.2.7.tgz";
      path = fetchurl {
        name = "fast_glob___fast_glob_3.2.7.tgz";
        url  = "https://registry.yarnpkg.com/fast-glob/-/fast-glob-3.2.7.tgz";
        sha1 = "fd6cb7a2d7e9aa7a7846111e85a196d6b2f766a1";
      };
    }
    {
      name = "fastq___fastq_1.13.0.tgz";
      path = fetchurl {
        name = "fastq___fastq_1.13.0.tgz";
        url  = "https://registry.yarnpkg.com/fastq/-/fastq-1.13.0.tgz";
        sha1 = "616760f88a7526bdfc596b7cab8c18938c36b98c";
      };
    }
    {
      name = "fill_range___fill_range_7.0.1.tgz";
      path = fetchurl {
        name = "fill_range___fill_range_7.0.1.tgz";
        url  = "https://registry.yarnpkg.com/fill-range/-/fill-range-7.0.1.tgz";
        sha1 = "1919a6a7c75fe38b2c7c77e5198535da9acdda40";
      };
    }
    {
      name = "fraction.js___fraction.js_4.1.1.tgz";
      path = fetchurl {
        name = "fraction.js___fraction.js_4.1.1.tgz";
        url  = "https://registry.yarnpkg.com/fraction.js/-/fraction.js-4.1.1.tgz";
        sha1 = "ac4e520473dae67012d618aab91eda09bcb400ff";
      };
    }
    {
      name = "fs_extra___fs_extra_5.0.0.tgz";
      path = fetchurl {
        name = "fs_extra___fs_extra_5.0.0.tgz";
        url  = "https://registry.yarnpkg.com/fs-extra/-/fs-extra-5.0.0.tgz";
        sha1 = "414d0110cdd06705734d055652c5411260c31abd";
      };
    }
    {
      name = "fs_extra___fs_extra_9.1.0.tgz";
      path = fetchurl {
        name = "fs_extra___fs_extra_9.1.0.tgz";
        url  = "https://registry.yarnpkg.com/fs-extra/-/fs-extra-9.1.0.tgz";
        sha1 = "5954460c764a8da2094ba3554bf839e6b9a7c86d";
      };
    }
    {
      name = "fs.realpath___fs.realpath_1.0.0.tgz";
      path = fetchurl {
        name = "fs.realpath___fs.realpath_1.0.0.tgz";
        url  = "https://registry.yarnpkg.com/fs.realpath/-/fs.realpath-1.0.0.tgz";
        sha1 = "1504ad2523158caa40db4a2787cb01411994ea4f";
      };
    }
    {
      name = "fsevents___fsevents_2.3.2.tgz";
      path = fetchurl {
        name = "fsevents___fsevents_2.3.2.tgz";
        url  = "https://registry.yarnpkg.com/fsevents/-/fsevents-2.3.2.tgz";
        sha1 = "8a526f78b8fdf4623b709e0b975c52c24c02fd1a";
      };
    }
    {
      name = "function_bind___function_bind_1.1.1.tgz";
      path = fetchurl {
        name = "function_bind___function_bind_1.1.1.tgz";
        url  = "https://registry.yarnpkg.com/function-bind/-/function-bind-1.1.1.tgz";
        sha1 = "a56899d3ea3c9bab874bb9773b7c5ede92f4895d";
      };
    }
    {
      name = "get_stream___get_stream_4.1.0.tgz";
      path = fetchurl {
        name = "get_stream___get_stream_4.1.0.tgz";
        url  = "https://registry.yarnpkg.com/get-stream/-/get-stream-4.1.0.tgz";
        sha1 = "c1b255575f3dc21d59bfc79cd3d2b46b1c3a54b5";
      };
    }
    {
      name = "get_stream___get_stream_5.2.0.tgz";
      path = fetchurl {
        name = "get_stream___get_stream_5.2.0.tgz";
        url  = "https://registry.yarnpkg.com/get-stream/-/get-stream-5.2.0.tgz";
        sha1 = "4966a1795ee5ace65e706c4b7beb71257d6e22d3";
      };
    }
    {
      name = "glob_base___glob_base_0.3.0.tgz";
      path = fetchurl {
        name = "glob_base___glob_base_0.3.0.tgz";
        url  = "https://registry.yarnpkg.com/glob-base/-/glob-base-0.3.0.tgz";
        sha1 = "dbb164f6221b1c0b1ccf82aea328b497df0ea3c4";
      };
    }
    {
      name = "glob_parent___glob_parent_2.0.0.tgz";
      path = fetchurl {
        name = "glob_parent___glob_parent_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/glob-parent/-/glob-parent-2.0.0.tgz";
        sha1 = "81383d72db054fcccf5336daa902f182f6edbb28";
      };
    }
    {
      name = "glob_parent___glob_parent_5.1.2.tgz";
      path = fetchurl {
        name = "glob_parent___glob_parent_5.1.2.tgz";
        url  = "https://registry.yarnpkg.com/glob-parent/-/glob-parent-5.1.2.tgz";
        sha1 = "869832c58034fe68a4093c17dc15e8340d8401c4";
      };
    }
    {
      name = "glob___glob_7.1.2.tgz";
      path = fetchurl {
        name = "glob___glob_7.1.2.tgz";
        url  = "https://registry.yarnpkg.com/glob/-/glob-7.1.2.tgz";
        sha1 = "c19c9df9a028702d678612384a6552404c636d15";
      };
    }
    {
      name = "glob___glob_7.2.0.tgz";
      path = fetchurl {
        name = "glob___glob_7.2.0.tgz";
        url  = "https://registry.yarnpkg.com/glob/-/glob-7.2.0.tgz";
        sha1 = "d15535af7732e02e948f4c41628bd910293f6023";
      };
    }
    {
      name = "global_dirs___global_dirs_2.1.0.tgz";
      path = fetchurl {
        name = "global_dirs___global_dirs_2.1.0.tgz";
        url  = "https://registry.yarnpkg.com/global-dirs/-/global-dirs-2.1.0.tgz";
        sha1 = "e9046a49c806ff04d6c1825e196c8f0091e8df4d";
      };
    }
    {
      name = "got___got_9.6.0.tgz";
      path = fetchurl {
        name = "got___got_9.6.0.tgz";
        url  = "https://registry.yarnpkg.com/got/-/got-9.6.0.tgz";
        sha1 = "edf45e7d67f99545705de1f7bbeeeb121765ed85";
      };
    }
    {
      name = "graceful_fs___graceful_fs_4.2.8.tgz";
      path = fetchurl {
        name = "graceful_fs___graceful_fs_4.2.8.tgz";
        url  = "https://registry.yarnpkg.com/graceful-fs/-/graceful-fs-4.2.8.tgz";
        sha1 = "e412b8d33f5e006593cbd3cee6df9f2cebbe802a";
      };
    }
    {
      name = "growl___growl_1.10.5.tgz";
      path = fetchurl {
        name = "growl___growl_1.10.5.tgz";
        url  = "https://registry.yarnpkg.com/growl/-/growl-1.10.5.tgz";
        sha1 = "f2735dc2283674fa67478b10181059355c369e5e";
      };
    }
    {
      name = "has_flag___has_flag_3.0.0.tgz";
      path = fetchurl {
        name = "has_flag___has_flag_3.0.0.tgz";
        url  = "https://registry.yarnpkg.com/has-flag/-/has-flag-3.0.0.tgz";
        sha1 = "b5d454dc2199ae225699f3467e5a07f3b955bafd";
      };
    }
    {
      name = "has_flag___has_flag_4.0.0.tgz";
      path = fetchurl {
        name = "has_flag___has_flag_4.0.0.tgz";
        url  = "https://registry.yarnpkg.com/has-flag/-/has-flag-4.0.0.tgz";
        sha1 = "944771fd9c81c81265c4d6941860da06bb59479b";
      };
    }
    {
      name = "has_yarn___has_yarn_2.1.0.tgz";
      path = fetchurl {
        name = "has_yarn___has_yarn_2.1.0.tgz";
        url  = "https://registry.yarnpkg.com/has-yarn/-/has-yarn-2.1.0.tgz";
        sha1 = "137e11354a7b5bf11aa5cb649cf0c6f3ff2b2e77";
      };
    }
    {
      name = "has___has_1.0.3.tgz";
      path = fetchurl {
        name = "has___has_1.0.3.tgz";
        url  = "https://registry.yarnpkg.com/has/-/has-1.0.3.tgz";
        sha1 = "722d7cbfc1f6aa8241f16dd814e011e1f41e8796";
      };
    }
    {
      name = "he___he_1.1.1.tgz";
      path = fetchurl {
        name = "he___he_1.1.1.tgz";
        url  = "https://registry.yarnpkg.com/he/-/he-1.1.1.tgz";
        sha1 = "93410fd21b009735151f8868c2f271f3427e23fd";
      };
    }
    {
      name = "html_tags___html_tags_3.1.0.tgz";
      path = fetchurl {
        name = "html_tags___html_tags_3.1.0.tgz";
        url  = "https://registry.yarnpkg.com/html-tags/-/html-tags-3.1.0.tgz";
        sha1 = "7b5e6f7e665e9fb41f30007ed9e0d41e97fb2140";
      };
    }
    {
      name = "htmlparser2___htmlparser2_3.10.1.tgz";
      path = fetchurl {
        name = "htmlparser2___htmlparser2_3.10.1.tgz";
        url  = "https://registry.yarnpkg.com/htmlparser2/-/htmlparser2-3.10.1.tgz";
        sha1 = "bd679dc3f59897b6a34bb10749c855bb53a9392f";
      };
    }
    {
      name = "http_cache_semantics___http_cache_semantics_4.1.0.tgz";
      path = fetchurl {
        name = "http_cache_semantics___http_cache_semantics_4.1.0.tgz";
        url  = "https://registry.yarnpkg.com/http-cache-semantics/-/http-cache-semantics-4.1.0.tgz";
        sha1 = "49e91c5cbf36c9b94bcfcd71c23d5249ec74e390";
      };
    }
    {
      name = "i18next___i18next_11.10.2.tgz";
      path = fetchurl {
        name = "i18next___i18next_11.10.2.tgz";
        url  = "https://registry.yarnpkg.com/i18next/-/i18next-11.10.2.tgz";
        sha1 = "e5f10346f6320ecf15595419926c25255381a56c";
      };
    }
    {
      name = "ignore_by_default___ignore_by_default_1.0.1.tgz";
      path = fetchurl {
        name = "ignore_by_default___ignore_by_default_1.0.1.tgz";
        url  = "https://registry.yarnpkg.com/ignore-by-default/-/ignore-by-default-1.0.1.tgz";
        sha1 = "48ca6d72f6c6a3af00a9ad4ae6876be3889e2b09";
      };
    }
    {
      name = "import_lazy___import_lazy_2.1.0.tgz";
      path = fetchurl {
        name = "import_lazy___import_lazy_2.1.0.tgz";
        url  = "https://registry.yarnpkg.com/import-lazy/-/import-lazy-2.1.0.tgz";
        sha1 = "05698e3d45c88e8d7e9d92cb0584e77f096f3e43";
      };
    }
    {
      name = "imurmurhash___imurmurhash_0.1.4.tgz";
      path = fetchurl {
        name = "imurmurhash___imurmurhash_0.1.4.tgz";
        url  = "https://registry.yarnpkg.com/imurmurhash/-/imurmurhash-0.1.4.tgz";
        sha1 = "9218b9b2b928a238b13dc4fb6b6d576f231453ea";
      };
    }
    {
      name = "inflight___inflight_1.0.6.tgz";
      path = fetchurl {
        name = "inflight___inflight_1.0.6.tgz";
        url  = "https://registry.yarnpkg.com/inflight/-/inflight-1.0.6.tgz";
        sha1 = "49bd6331d7d02d0c09bc910a1075ba8165b56df9";
      };
    }
    {
      name = "inherits___inherits_2.0.4.tgz";
      path = fetchurl {
        name = "inherits___inherits_2.0.4.tgz";
        url  = "https://registry.yarnpkg.com/inherits/-/inherits-2.0.4.tgz";
        sha1 = "0fa2c64f932917c3433a0ded55363aae37416b7c";
      };
    }
    {
      name = "ini___ini_1.3.7.tgz";
      path = fetchurl {
        name = "ini___ini_1.3.7.tgz";
        url  = "https://registry.yarnpkg.com/ini/-/ini-1.3.7.tgz";
        sha1 = "a09363e1911972ea16d7a8851005d84cf09a9a84";
      };
    }
    {
      name = "ini___ini_1.3.8.tgz";
      path = fetchurl {
        name = "ini___ini_1.3.8.tgz";
        url  = "https://registry.yarnpkg.com/ini/-/ini-1.3.8.tgz";
        sha1 = "a29da425b48806f34767a4efce397269af28432c";
      };
    }
    {
      name = "is_arrayish___is_arrayish_0.3.2.tgz";
      path = fetchurl {
        name = "is_arrayish___is_arrayish_0.3.2.tgz";
        url  = "https://registry.yarnpkg.com/is-arrayish/-/is-arrayish-0.3.2.tgz";
        sha1 = "4574a2ae56f7ab206896fb431eaeed066fdf8f03";
      };
    }
    {
      name = "is_binary_path___is_binary_path_2.1.0.tgz";
      path = fetchurl {
        name = "is_binary_path___is_binary_path_2.1.0.tgz";
        url  = "https://registry.yarnpkg.com/is-binary-path/-/is-binary-path-2.1.0.tgz";
        sha1 = "ea1f7f3b80f064236e83470f86c09c254fb45b09";
      };
    }
    {
      name = "is_ci___is_ci_2.0.0.tgz";
      path = fetchurl {
        name = "is_ci___is_ci_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/is-ci/-/is-ci-2.0.0.tgz";
        sha1 = "6bc6334181810e04b5c22b3d589fdca55026404c";
      };
    }
    {
      name = "is_core_module___is_core_module_2.6.0.tgz";
      path = fetchurl {
        name = "is_core_module___is_core_module_2.6.0.tgz";
        url  = "https://registry.yarnpkg.com/is-core-module/-/is-core-module-2.6.0.tgz";
        sha1 = "d7553b2526fe59b92ba3e40c8df757ec8a709e19";
      };
    }
    {
      name = "is_dotfile___is_dotfile_1.0.3.tgz";
      path = fetchurl {
        name = "is_dotfile___is_dotfile_1.0.3.tgz";
        url  = "https://registry.yarnpkg.com/is-dotfile/-/is-dotfile-1.0.3.tgz";
        sha1 = "a6a2f32ffd2dfb04f5ca25ecd0f6b83cf798a1e1";
      };
    }
    {
      name = "is_extglob___is_extglob_1.0.0.tgz";
      path = fetchurl {
        name = "is_extglob___is_extglob_1.0.0.tgz";
        url  = "https://registry.yarnpkg.com/is-extglob/-/is-extglob-1.0.0.tgz";
        sha1 = "ac468177c4943405a092fc8f29760c6ffc6206c0";
      };
    }
    {
      name = "is_extglob___is_extglob_2.1.1.tgz";
      path = fetchurl {
        name = "is_extglob___is_extglob_2.1.1.tgz";
        url  = "https://registry.yarnpkg.com/is-extglob/-/is-extglob-2.1.1.tgz";
        sha1 = "a88c02535791f02ed37c76a1b9ea9773c833f8c2";
      };
    }
    {
      name = "is_fullwidth_code_point___is_fullwidth_code_point_2.0.0.tgz";
      path = fetchurl {
        name = "is_fullwidth_code_point___is_fullwidth_code_point_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/is-fullwidth-code-point/-/is-fullwidth-code-point-2.0.0.tgz";
        sha1 = "a3b30a5c4f199183167aaab93beefae3ddfb654f";
      };
    }
    {
      name = "is_fullwidth_code_point___is_fullwidth_code_point_3.0.0.tgz";
      path = fetchurl {
        name = "is_fullwidth_code_point___is_fullwidth_code_point_3.0.0.tgz";
        url  = "https://registry.yarnpkg.com/is-fullwidth-code-point/-/is-fullwidth-code-point-3.0.0.tgz";
        sha1 = "f116f8064fe90b3f7844a38997c0b75051269f1d";
      };
    }
    {
      name = "is_glob___is_glob_2.0.1.tgz";
      path = fetchurl {
        name = "is_glob___is_glob_2.0.1.tgz";
        url  = "https://registry.yarnpkg.com/is-glob/-/is-glob-2.0.1.tgz";
        sha1 = "d096f926a3ded5600f3fdfd91198cb0888c2d863";
      };
    }
    {
      name = "is_glob___is_glob_4.0.1.tgz";
      path = fetchurl {
        name = "is_glob___is_glob_4.0.1.tgz";
        url  = "https://registry.yarnpkg.com/is-glob/-/is-glob-4.0.1.tgz";
        sha1 = "7567dbe9f2f5e2467bc77ab83c4a29482407a5dc";
      };
    }
    {
      name = "is_installed_globally___is_installed_globally_0.3.2.tgz";
      path = fetchurl {
        name = "is_installed_globally___is_installed_globally_0.3.2.tgz";
        url  = "https://registry.yarnpkg.com/is-installed-globally/-/is-installed-globally-0.3.2.tgz";
        sha1 = "fd3efa79ee670d1187233182d5b0a1dd00313141";
      };
    }
    {
      name = "is_npm___is_npm_4.0.0.tgz";
      path = fetchurl {
        name = "is_npm___is_npm_4.0.0.tgz";
        url  = "https://registry.yarnpkg.com/is-npm/-/is-npm-4.0.0.tgz";
        sha1 = "c90dd8380696df87a7a6d823c20d0b12bbe3c84d";
      };
    }
    {
      name = "is_number___is_number_7.0.0.tgz";
      path = fetchurl {
        name = "is_number___is_number_7.0.0.tgz";
        url  = "https://registry.yarnpkg.com/is-number/-/is-number-7.0.0.tgz";
        sha1 = "7535345b896734d5f80c4d06c50955527a14f12b";
      };
    }
    {
      name = "is_obj___is_obj_2.0.0.tgz";
      path = fetchurl {
        name = "is_obj___is_obj_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/is-obj/-/is-obj-2.0.0.tgz";
        sha1 = "473fb05d973705e3fd9620545018ca8e22ef4982";
      };
    }
    {
      name = "is_path_inside___is_path_inside_3.0.3.tgz";
      path = fetchurl {
        name = "is_path_inside___is_path_inside_3.0.3.tgz";
        url  = "https://registry.yarnpkg.com/is-path-inside/-/is-path-inside-3.0.3.tgz";
        sha1 = "d231362e53a07ff2b0e0ea7fed049161ffd16283";
      };
    }
    {
      name = "is_typedarray___is_typedarray_1.0.0.tgz";
      path = fetchurl {
        name = "is_typedarray___is_typedarray_1.0.0.tgz";
        url  = "https://registry.yarnpkg.com/is-typedarray/-/is-typedarray-1.0.0.tgz";
        sha1 = "e479c80858df0c1b11ddda6940f96011fcda4a9a";
      };
    }
    {
      name = "is_yarn_global___is_yarn_global_0.3.0.tgz";
      path = fetchurl {
        name = "is_yarn_global___is_yarn_global_0.3.0.tgz";
        url  = "https://registry.yarnpkg.com/is-yarn-global/-/is-yarn-global-0.3.0.tgz";
        sha1 = "d502d3382590ea3004893746754c89139973e232";
      };
    }
    {
      name = "js_yaml___js_yaml_3.14.1.tgz";
      path = fetchurl {
        name = "js_yaml___js_yaml_3.14.1.tgz";
        url  = "https://registry.yarnpkg.com/js-yaml/-/js-yaml-3.14.1.tgz";
        sha1 = "dae812fdb3825fa306609a8717383c50c36a0537";
      };
    }
    {
      name = "json_buffer___json_buffer_3.0.0.tgz";
      path = fetchurl {
        name = "json_buffer___json_buffer_3.0.0.tgz";
        url  = "https://registry.yarnpkg.com/json-buffer/-/json-buffer-3.0.0.tgz";
        sha1 = "5b1f397afc75d677bde8bcfc0e47e1f9a3d9a898";
      };
    }
    {
      name = "jsonfile___jsonfile_4.0.0.tgz";
      path = fetchurl {
        name = "jsonfile___jsonfile_4.0.0.tgz";
        url  = "https://registry.yarnpkg.com/jsonfile/-/jsonfile-4.0.0.tgz";
        sha1 = "8771aae0799b64076b76640fca058f9c10e33ecb";
      };
    }
    {
      name = "jsonfile___jsonfile_6.1.0.tgz";
      path = fetchurl {
        name = "jsonfile___jsonfile_6.1.0.tgz";
        url  = "https://registry.yarnpkg.com/jsonfile/-/jsonfile-6.1.0.tgz";
        sha1 = "bc55b2634793c679ec6403094eb13698a6ec0aae";
      };
    }
    {
      name = "keyv___keyv_3.1.0.tgz";
      path = fetchurl {
        name = "keyv___keyv_3.1.0.tgz";
        url  = "https://registry.yarnpkg.com/keyv/-/keyv-3.1.0.tgz";
        sha1 = "ecc228486f69991e49e9476485a5be1e8fc5c4d9";
      };
    }
    {
      name = "latest_version___latest_version_5.1.0.tgz";
      path = fetchurl {
        name = "latest_version___latest_version_5.1.0.tgz";
        url  = "https://registry.yarnpkg.com/latest-version/-/latest-version-5.1.0.tgz";
        sha1 = "119dfe908fe38d15dfa43ecd13fa12ec8832face";
      };
    }
    {
      name = "lodash.assignin___lodash.assignin_4.2.0.tgz";
      path = fetchurl {
        name = "lodash.assignin___lodash.assignin_4.2.0.tgz";
        url  = "https://registry.yarnpkg.com/lodash.assignin/-/lodash.assignin-4.2.0.tgz";
        sha1 = "ba8df5fb841eb0a3e8044232b0e263a8dc6a28a2";
      };
    }
    {
      name = "lodash.bind___lodash.bind_4.2.1.tgz";
      path = fetchurl {
        name = "lodash.bind___lodash.bind_4.2.1.tgz";
        url  = "https://registry.yarnpkg.com/lodash.bind/-/lodash.bind-4.2.1.tgz";
        sha1 = "7ae3017e939622ac31b7d7d7dcb1b34db1690d35";
      };
    }
    {
      name = "lodash.defaults___lodash.defaults_4.2.0.tgz";
      path = fetchurl {
        name = "lodash.defaults___lodash.defaults_4.2.0.tgz";
        url  = "https://registry.yarnpkg.com/lodash.defaults/-/lodash.defaults-4.2.0.tgz";
        sha1 = "d09178716ffea4dde9e5fb7b37f6f0802274580c";
      };
    }
    {
      name = "lodash.filter___lodash.filter_4.6.0.tgz";
      path = fetchurl {
        name = "lodash.filter___lodash.filter_4.6.0.tgz";
        url  = "https://registry.yarnpkg.com/lodash.filter/-/lodash.filter-4.6.0.tgz";
        sha1 = "668b1d4981603ae1cc5a6fa760143e480b4c4ace";
      };
    }
    {
      name = "lodash.flatten___lodash.flatten_4.4.0.tgz";
      path = fetchurl {
        name = "lodash.flatten___lodash.flatten_4.4.0.tgz";
        url  = "https://registry.yarnpkg.com/lodash.flatten/-/lodash.flatten-4.4.0.tgz";
        sha1 = "f31c22225a9632d2bbf8e4addbef240aa765a61f";
      };
    }
    {
      name = "lodash.foreach___lodash.foreach_4.5.0.tgz";
      path = fetchurl {
        name = "lodash.foreach___lodash.foreach_4.5.0.tgz";
        url  = "https://registry.yarnpkg.com/lodash.foreach/-/lodash.foreach-4.5.0.tgz";
        sha1 = "1a6a35eace401280c7f06dddec35165ab27e3e53";
      };
    }
    {
      name = "lodash.map___lodash.map_4.6.0.tgz";
      path = fetchurl {
        name = "lodash.map___lodash.map_4.6.0.tgz";
        url  = "https://registry.yarnpkg.com/lodash.map/-/lodash.map-4.6.0.tgz";
        sha1 = "771ec7839e3473d9c4cde28b19394c3562f4f6d3";
      };
    }
    {
      name = "lodash.merge___lodash.merge_4.6.2.tgz";
      path = fetchurl {
        name = "lodash.merge___lodash.merge_4.6.2.tgz";
        url  = "https://registry.yarnpkg.com/lodash.merge/-/lodash.merge-4.6.2.tgz";
        sha1 = "558aa53b43b661e1925a0afdfa36a9a1085fe57a";
      };
    }
    {
      name = "lodash.pick___lodash.pick_4.4.0.tgz";
      path = fetchurl {
        name = "lodash.pick___lodash.pick_4.4.0.tgz";
        url  = "https://registry.yarnpkg.com/lodash.pick/-/lodash.pick-4.4.0.tgz";
        sha1 = "52f05610fff9ded422611441ed1fc123a03001b3";
      };
    }
    {
      name = "lodash.reduce___lodash.reduce_4.6.0.tgz";
      path = fetchurl {
        name = "lodash.reduce___lodash.reduce_4.6.0.tgz";
        url  = "https://registry.yarnpkg.com/lodash.reduce/-/lodash.reduce-4.6.0.tgz";
        sha1 = "f1ab6b839299ad48f784abbf476596f03b914d3b";
      };
    }
    {
      name = "lodash.reject___lodash.reject_4.6.0.tgz";
      path = fetchurl {
        name = "lodash.reject___lodash.reject_4.6.0.tgz";
        url  = "https://registry.yarnpkg.com/lodash.reject/-/lodash.reject-4.6.0.tgz";
        sha1 = "80d6492dc1470864bbf583533b651f42a9f52415";
      };
    }
    {
      name = "lodash.some___lodash.some_4.6.0.tgz";
      path = fetchurl {
        name = "lodash.some___lodash.some_4.6.0.tgz";
        url  = "https://registry.yarnpkg.com/lodash.some/-/lodash.some-4.6.0.tgz";
        sha1 = "1bb9f314ef6b8baded13b549169b2a945eb68e4d";
      };
    }
    {
      name = "lodash.topath___lodash.topath_4.5.2.tgz";
      path = fetchurl {
        name = "lodash.topath___lodash.topath_4.5.2.tgz";
        url  = "https://registry.yarnpkg.com/lodash.topath/-/lodash.topath-4.5.2.tgz";
        sha1 = "3616351f3bba61994a0931989660bd03254fd009";
      };
    }
    {
      name = "lodash___lodash_4.17.21.tgz";
      path = fetchurl {
        name = "lodash___lodash_4.17.21.tgz";
        url  = "https://registry.yarnpkg.com/lodash/-/lodash-4.17.21.tgz";
        sha1 = "679591c564c3bffaae8454cf0b3df370c3d6911c";
      };
    }
    {
      name = "lowercase_keys___lowercase_keys_1.0.1.tgz";
      path = fetchurl {
        name = "lowercase_keys___lowercase_keys_1.0.1.tgz";
        url  = "https://registry.yarnpkg.com/lowercase-keys/-/lowercase-keys-1.0.1.tgz";
        sha1 = "6f9e30b47084d971a7c820ff15a6c5167b74c26f";
      };
    }
    {
      name = "lowercase_keys___lowercase_keys_2.0.0.tgz";
      path = fetchurl {
        name = "lowercase_keys___lowercase_keys_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/lowercase-keys/-/lowercase-keys-2.0.0.tgz";
        sha1 = "2603e78b7b4b0006cbca2fbcc8a3202558ac9479";
      };
    }
    {
      name = "make_dir___make_dir_3.1.0.tgz";
      path = fetchurl {
        name = "make_dir___make_dir_3.1.0.tgz";
        url  = "https://registry.yarnpkg.com/make-dir/-/make-dir-3.1.0.tgz";
        sha1 = "415e967046b3a7f1d185277d84aa58203726a13f";
      };
    }
    {
      name = "merge2___merge2_1.4.1.tgz";
      path = fetchurl {
        name = "merge2___merge2_1.4.1.tgz";
        url  = "https://registry.yarnpkg.com/merge2/-/merge2-1.4.1.tgz";
        sha1 = "4368892f885e907455a6fd7dc55c0c9d404990ae";
      };
    }
    {
      name = "micromatch___micromatch_4.0.4.tgz";
      path = fetchurl {
        name = "micromatch___micromatch_4.0.4.tgz";
        url  = "https://registry.yarnpkg.com/micromatch/-/micromatch-4.0.4.tgz";
        sha1 = "896d519dfe9db25fce94ceb7a500919bf881ebf9";
      };
    }
    {
      name = "mimic_response___mimic_response_1.0.1.tgz";
      path = fetchurl {
        name = "mimic_response___mimic_response_1.0.1.tgz";
        url  = "https://registry.yarnpkg.com/mimic-response/-/mimic-response-1.0.1.tgz";
        sha1 = "4923538878eef42063cb8a3e3b0798781487ab1b";
      };
    }
    {
      name = "minimatch___minimatch_3.0.4.tgz";
      path = fetchurl {
        name = "minimatch___minimatch_3.0.4.tgz";
        url  = "https://registry.yarnpkg.com/minimatch/-/minimatch-3.0.4.tgz";
        sha1 = "5166e286457f03306064be5497e8dbb0c3d32083";
      };
    }
    {
      name = "minimist___minimist_0.0.8.tgz";
      path = fetchurl {
        name = "minimist___minimist_0.0.8.tgz";
        url  = "https://registry.yarnpkg.com/minimist/-/minimist-0.0.8.tgz";
        sha1 = "857fcabfc3397d2625b8228262e86aa7a011b05d";
      };
    }
    {
      name = "minimist___minimist_1.2.5.tgz";
      path = fetchurl {
        name = "minimist___minimist_1.2.5.tgz";
        url  = "https://registry.yarnpkg.com/minimist/-/minimist-1.2.5.tgz";
        sha1 = "67d66014b66a6a8aaa0c083c5fd58df4e4e97602";
      };
    }
    {
      name = "mkdirp___mkdirp_0.5.1.tgz";
      path = fetchurl {
        name = "mkdirp___mkdirp_0.5.1.tgz";
        url  = "https://registry.yarnpkg.com/mkdirp/-/mkdirp-0.5.1.tgz";
        sha1 = "30057438eac6cf7f8c4767f38648d6697d75c903";
      };
    }
    {
      name = "mocha___mocha_5.2.0.tgz";
      path = fetchurl {
        name = "mocha___mocha_5.2.0.tgz";
        url  = "https://registry.yarnpkg.com/mocha/-/mocha-5.2.0.tgz";
        sha1 = "6d8ae508f59167f940f2b5b3c4a612ae50c90ae6";
      };
    }
    {
      name = "modern_normalize___modern_normalize_1.1.0.tgz";
      path = fetchurl {
        name = "modern_normalize___modern_normalize_1.1.0.tgz";
        url  = "https://registry.yarnpkg.com/modern-normalize/-/modern-normalize-1.1.0.tgz";
        sha1 = "da8e80140d9221426bd4f725c6e11283d34f90b7";
      };
    }
    {
      name = "ms___ms_2.0.0.tgz";
      path = fetchurl {
        name = "ms___ms_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/ms/-/ms-2.0.0.tgz";
        sha1 = "5608aeadfc00be6c2901df5f9861788de0d597c8";
      };
    }
    {
      name = "ms___ms_2.1.3.tgz";
      path = fetchurl {
        name = "ms___ms_2.1.3.tgz";
        url  = "https://registry.yarnpkg.com/ms/-/ms-2.1.3.tgz";
        sha1 = "574c8138ce1d2b5861f0b44579dbadd60c6615b2";
      };
    }
    {
      name = "nanocolors___nanocolors_0.1.12.tgz";
      path = fetchurl {
        name = "nanocolors___nanocolors_0.1.12.tgz";
        url  = "https://registry.yarnpkg.com/nanocolors/-/nanocolors-0.1.12.tgz";
        sha1 = "8577482c58cbd7b5bb1681db4cf48f11a87fd5f6";
      };
    }
    {
      name = "nanoid___nanoid_3.1.25.tgz";
      path = fetchurl {
        name = "nanoid___nanoid_3.1.25.tgz";
        url  = "https://registry.yarnpkg.com/nanoid/-/nanoid-3.1.25.tgz";
        sha1 = "09ca32747c0e543f0e1814b7d3793477f9c8e152";
      };
    }
    {
      name = "node_emoji___node_emoji_1.11.0.tgz";
      path = fetchurl {
        name = "node_emoji___node_emoji_1.11.0.tgz";
        url  = "https://registry.yarnpkg.com/node-emoji/-/node-emoji-1.11.0.tgz";
        sha1 = "69a0150e6946e2f115e9d7ea4df7971e2628301c";
      };
    }
    {
      name = "node_releases___node_releases_1.1.76.tgz";
      path = fetchurl {
        name = "node_releases___node_releases_1.1.76.tgz";
        url  = "https://registry.yarnpkg.com/node-releases/-/node-releases-1.1.76.tgz";
        sha1 = "df245b062b0cafbd5282ab6792f7dccc2d97f36e";
      };
    }
    {
      name = "nodemon___nodemon_2.0.7.tgz";
      path = fetchurl {
        name = "nodemon___nodemon_2.0.7.tgz";
        url  = "https://registry.yarnpkg.com/nodemon/-/nodemon-2.0.7.tgz";
        sha1 = "6f030a0a0ebe3ea1ba2a38f71bf9bab4841ced32";
      };
    }
    {
      name = "nopt___nopt_1.0.10.tgz";
      path = fetchurl {
        name = "nopt___nopt_1.0.10.tgz";
        url  = "https://registry.yarnpkg.com/nopt/-/nopt-1.0.10.tgz";
        sha1 = "6ddd21bd2a31417b92727dd585f8a6f37608ebee";
      };
    }
    {
      name = "normalize_path___normalize_path_3.0.0.tgz";
      path = fetchurl {
        name = "normalize_path___normalize_path_3.0.0.tgz";
        url  = "https://registry.yarnpkg.com/normalize-path/-/normalize-path-3.0.0.tgz";
        sha1 = "0dcd69ff23a1c9b11fd0978316644a0388216a65";
      };
    }
    {
      name = "normalize_range___normalize_range_0.1.2.tgz";
      path = fetchurl {
        name = "normalize_range___normalize_range_0.1.2.tgz";
        url  = "https://registry.yarnpkg.com/normalize-range/-/normalize-range-0.1.2.tgz";
        sha1 = "2d10c06bdfd312ea9777695a4d28439456b75942";
      };
    }
    {
      name = "normalize_url___normalize_url_4.5.1.tgz";
      path = fetchurl {
        name = "normalize_url___normalize_url_4.5.1.tgz";
        url  = "https://registry.yarnpkg.com/normalize-url/-/normalize-url-4.5.1.tgz";
        sha1 = "0dd90cf1288ee1d1313b87081c9a5932ee48518a";
      };
    }
    {
      name = "nth_check___nth_check_1.0.2.tgz";
      path = fetchurl {
        name = "nth_check___nth_check_1.0.2.tgz";
        url  = "https://registry.yarnpkg.com/nth-check/-/nth-check-1.0.2.tgz";
        sha1 = "b2bd295c37e3dd58a3bf0700376663ba4d9cf05c";
      };
    }
    {
      name = "object_assign___object_assign_4.1.1.tgz";
      path = fetchurl {
        name = "object_assign___object_assign_4.1.1.tgz";
        url  = "https://registry.yarnpkg.com/object-assign/-/object-assign-4.1.1.tgz";
        sha1 = "2109adc7965887cfc05cbbd442cac8bfbb360863";
      };
    }
    {
      name = "object_hash___object_hash_2.2.0.tgz";
      path = fetchurl {
        name = "object_hash___object_hash_2.2.0.tgz";
        url  = "https://registry.yarnpkg.com/object-hash/-/object-hash-2.2.0.tgz";
        sha1 = "5ad518581eefc443bd763472b8ff2e9c2c0d54a5";
      };
    }
    {
      name = "once___once_1.4.0.tgz";
      path = fetchurl {
        name = "once___once_1.4.0.tgz";
        url  = "https://registry.yarnpkg.com/once/-/once-1.4.0.tgz";
        sha1 = "583b1aa775961d4b113ac17d9c50baef9dd76bd1";
      };
    }
    {
      name = "p_cancelable___p_cancelable_1.1.0.tgz";
      path = fetchurl {
        name = "p_cancelable___p_cancelable_1.1.0.tgz";
        url  = "https://registry.yarnpkg.com/p-cancelable/-/p-cancelable-1.1.0.tgz";
        sha1 = "d078d15a3af409220c886f1d9a0ca2e441ab26cc";
      };
    }
    {
      name = "package_json___package_json_6.5.0.tgz";
      path = fetchurl {
        name = "package_json___package_json_6.5.0.tgz";
        url  = "https://registry.yarnpkg.com/package-json/-/package-json-6.5.0.tgz";
        sha1 = "6feedaca35e75725876d0b0e64974697fed145b0";
      };
    }
    {
      name = "parse_glob___parse_glob_3.0.4.tgz";
      path = fetchurl {
        name = "parse_glob___parse_glob_3.0.4.tgz";
        url  = "https://registry.yarnpkg.com/parse-glob/-/parse-glob-3.0.4.tgz";
        sha1 = "b2c376cfb11f35513badd173ef0bb6e3a388391c";
      };
    }
    {
      name = "path_is_absolute___path_is_absolute_1.0.1.tgz";
      path = fetchurl {
        name = "path_is_absolute___path_is_absolute_1.0.1.tgz";
        url  = "https://registry.yarnpkg.com/path-is-absolute/-/path-is-absolute-1.0.1.tgz";
        sha1 = "174b9268735534ffbc7ace6bf53a5a9e1b5c5f5f";
      };
    }
    {
      name = "path_parse___path_parse_1.0.7.tgz";
      path = fetchurl {
        name = "path_parse___path_parse_1.0.7.tgz";
        url  = "https://registry.yarnpkg.com/path-parse/-/path-parse-1.0.7.tgz";
        sha1 = "fbc114b60ca42b30d9daf5858e4bd68bbedb6735";
      };
    }
    {
      name = "picomatch___picomatch_2.3.0.tgz";
      path = fetchurl {
        name = "picomatch___picomatch_2.3.0.tgz";
        url  = "https://registry.yarnpkg.com/picomatch/-/picomatch-2.3.0.tgz";
        sha1 = "f1f061de8f6a4bf022892e2d128234fb98302972";
      };
    }
    {
      name = "postcss_functions___postcss_functions_3.0.0.tgz";
      path = fetchurl {
        name = "postcss_functions___postcss_functions_3.0.0.tgz";
        url  = "https://registry.yarnpkg.com/postcss-functions/-/postcss-functions-3.0.0.tgz";
        sha1 = "0e94d01444700a481de20de4d55fb2640564250e";
      };
    }
    {
      name = "postcss_js___postcss_js_3.0.3.tgz";
      path = fetchurl {
        name = "postcss_js___postcss_js_3.0.3.tgz";
        url  = "https://registry.yarnpkg.com/postcss-js/-/postcss-js-3.0.3.tgz";
        sha1 = "2f0bd370a2e8599d45439f6970403b5873abda33";
      };
    }
    {
      name = "postcss_nested___postcss_nested_5.0.5.tgz";
      path = fetchurl {
        name = "postcss_nested___postcss_nested_5.0.5.tgz";
        url  = "https://registry.yarnpkg.com/postcss-nested/-/postcss-nested-5.0.5.tgz";
        sha1 = "f0a107d33a9fab11d7637205f5321e27223e3603";
      };
    }
    {
      name = "postcss_selector_parser___postcss_selector_parser_6.0.6.tgz";
      path = fetchurl {
        name = "postcss_selector_parser___postcss_selector_parser_6.0.6.tgz";
        url  = "https://registry.yarnpkg.com/postcss-selector-parser/-/postcss-selector-parser-6.0.6.tgz";
        sha1 = "2c5bba8174ac2f6981ab631a42ab0ee54af332ea";
      };
    }
    {
      name = "postcss_value_parser___postcss_value_parser_3.3.1.tgz";
      path = fetchurl {
        name = "postcss_value_parser___postcss_value_parser_3.3.1.tgz";
        url  = "https://registry.yarnpkg.com/postcss-value-parser/-/postcss-value-parser-3.3.1.tgz";
        sha1 = "9ff822547e2893213cf1c30efa51ac5fd1ba8281";
      };
    }
    {
      name = "postcss_value_parser___postcss_value_parser_4.1.0.tgz";
      path = fetchurl {
        name = "postcss_value_parser___postcss_value_parser_4.1.0.tgz";
        url  = "https://registry.yarnpkg.com/postcss-value-parser/-/postcss-value-parser-4.1.0.tgz";
        sha1 = "443f6a20ced6481a2bda4fa8532a6e55d789a2cb";
      };
    }
    {
      name = "postcss___postcss_8.2.10.tgz";
      path = fetchurl {
        name = "postcss___postcss_8.2.10.tgz";
        url  = "https://registry.yarnpkg.com/postcss/-/postcss-8.2.10.tgz";
        sha1 = "ca7a042aa8aff494b334d0ff3e9e77079f6f702b";
      };
    }
    {
      name = "postcss___postcss_6.0.23.tgz";
      path = fetchurl {
        name = "postcss___postcss_6.0.23.tgz";
        url  = "https://registry.yarnpkg.com/postcss/-/postcss-6.0.23.tgz";
        sha1 = "61c82cc328ac60e677645f979054eb98bc0e3324";
      };
    }
    {
      name = "postcss___postcss_8.3.7.tgz";
      path = fetchurl {
        name = "postcss___postcss_8.3.7.tgz";
        url  = "https://registry.yarnpkg.com/postcss/-/postcss-8.3.7.tgz";
        sha1 = "ec88563588c8da8e58e7226f7633b51ae221eeda";
      };
    }
    {
      name = "prepend_http___prepend_http_2.0.0.tgz";
      path = fetchurl {
        name = "prepend_http___prepend_http_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/prepend-http/-/prepend-http-2.0.0.tgz";
        sha1 = "e92434bfa5ea8c19f41cdfd401d741a3c819d897";
      };
    }
    {
      name = "pretty_hrtime___pretty_hrtime_1.0.3.tgz";
      path = fetchurl {
        name = "pretty_hrtime___pretty_hrtime_1.0.3.tgz";
        url  = "https://registry.yarnpkg.com/pretty-hrtime/-/pretty-hrtime-1.0.3.tgz";
        sha1 = "b7e3ea42435a4c9b2759d99e0f201eb195802ee1";
      };
    }
    {
      name = "pstree.remy___pstree.remy_1.1.8.tgz";
      path = fetchurl {
        name = "pstree.remy___pstree.remy_1.1.8.tgz";
        url  = "https://registry.yarnpkg.com/pstree.remy/-/pstree.remy-1.1.8.tgz";
        sha1 = "c242224f4a67c21f686839bbdb4ac282b8373d3a";
      };
    }
    {
      name = "pump___pump_3.0.0.tgz";
      path = fetchurl {
        name = "pump___pump_3.0.0.tgz";
        url  = "https://registry.yarnpkg.com/pump/-/pump-3.0.0.tgz";
        sha1 = "b4a2116815bde2f4e1ea602354e8c75565107a64";
      };
    }
    {
      name = "pupa___pupa_2.1.1.tgz";
      path = fetchurl {
        name = "pupa___pupa_2.1.1.tgz";
        url  = "https://registry.yarnpkg.com/pupa/-/pupa-2.1.1.tgz";
        sha1 = "f5e8fd4afc2c5d97828faa523549ed8744a20d62";
      };
    }
    {
      name = "purgecss___purgecss_3.1.3.tgz";
      path = fetchurl {
        name = "purgecss___purgecss_3.1.3.tgz";
        url  = "https://registry.yarnpkg.com/purgecss/-/purgecss-3.1.3.tgz";
        sha1 = "26987ec09d12eeadc318e22f6e5a9eb0be094f41";
      };
    }
    {
      name = "queue_microtask___queue_microtask_1.2.3.tgz";
      path = fetchurl {
        name = "queue_microtask___queue_microtask_1.2.3.tgz";
        url  = "https://registry.yarnpkg.com/queue-microtask/-/queue-microtask-1.2.3.tgz";
        sha1 = "4929228bbc724dfac43e0efb058caf7b6cfb6243";
      };
    }
    {
      name = "quick_lru___quick_lru_5.1.1.tgz";
      path = fetchurl {
        name = "quick_lru___quick_lru_5.1.1.tgz";
        url  = "https://registry.yarnpkg.com/quick-lru/-/quick-lru-5.1.1.tgz";
        sha1 = "366493e6b3e42a3a6885e2e99d18f80fb7a8c932";
      };
    }
    {
      name = "rc___rc_1.2.8.tgz";
      path = fetchurl {
        name = "rc___rc_1.2.8.tgz";
        url  = "https://registry.yarnpkg.com/rc/-/rc-1.2.8.tgz";
        sha1 = "cd924bf5200a075b83c188cd6b9e211b7fc0d3ed";
      };
    }
    {
      name = "readable_stream___readable_stream_3.6.0.tgz";
      path = fetchurl {
        name = "readable_stream___readable_stream_3.6.0.tgz";
        url  = "https://registry.yarnpkg.com/readable-stream/-/readable-stream-3.6.0.tgz";
        sha1 = "337bbda3adc0706bd3e024426a286d4b4b2c9198";
      };
    }
    {
      name = "readdirp___readdirp_3.6.0.tgz";
      path = fetchurl {
        name = "readdirp___readdirp_3.6.0.tgz";
        url  = "https://registry.yarnpkg.com/readdirp/-/readdirp-3.6.0.tgz";
        sha1 = "74a370bd857116e245b29cc97340cd431a02a6c7";
      };
    }
    {
      name = "reduce_css_calc___reduce_css_calc_2.1.8.tgz";
      path = fetchurl {
        name = "reduce_css_calc___reduce_css_calc_2.1.8.tgz";
        url  = "https://registry.yarnpkg.com/reduce-css-calc/-/reduce-css-calc-2.1.8.tgz";
        sha1 = "7ef8761a28d614980dc0c982f772c93f7a99de03";
      };
    }
    {
      name = "registry_auth_token___registry_auth_token_4.2.1.tgz";
      path = fetchurl {
        name = "registry_auth_token___registry_auth_token_4.2.1.tgz";
        url  = "https://registry.yarnpkg.com/registry-auth-token/-/registry-auth-token-4.2.1.tgz";
        sha1 = "6d7b4006441918972ccd5fedcd41dc322c79b250";
      };
    }
    {
      name = "registry_url___registry_url_5.1.0.tgz";
      path = fetchurl {
        name = "registry_url___registry_url_5.1.0.tgz";
        url  = "https://registry.yarnpkg.com/registry-url/-/registry-url-5.1.0.tgz";
        sha1 = "e98334b50d5434b81136b44ec638d9c2009c5009";
      };
    }
    {
      name = "resolve___resolve_1.20.0.tgz";
      path = fetchurl {
        name = "resolve___resolve_1.20.0.tgz";
        url  = "https://registry.yarnpkg.com/resolve/-/resolve-1.20.0.tgz";
        sha1 = "629a013fb3f70755d6f0b7935cc1c2c5378b1975";
      };
    }
    {
      name = "responselike___responselike_1.0.2.tgz";
      path = fetchurl {
        name = "responselike___responselike_1.0.2.tgz";
        url  = "https://registry.yarnpkg.com/responselike/-/responselike-1.0.2.tgz";
        sha1 = "918720ef3b631c5642be068f15ade5a46f4ba1e7";
      };
    }
    {
      name = "reusify___reusify_1.0.4.tgz";
      path = fetchurl {
        name = "reusify___reusify_1.0.4.tgz";
        url  = "https://registry.yarnpkg.com/reusify/-/reusify-1.0.4.tgz";
        sha1 = "90da382b1e126efc02146e90845a88db12925d76";
      };
    }
    {
      name = "run_parallel___run_parallel_1.2.0.tgz";
      path = fetchurl {
        name = "run_parallel___run_parallel_1.2.0.tgz";
        url  = "https://registry.yarnpkg.com/run-parallel/-/run-parallel-1.2.0.tgz";
        sha1 = "66d1368da7bdf921eb9d95bd1a9229e7f21a43ee";
      };
    }
    {
      name = "safe_buffer___safe_buffer_5.2.1.tgz";
      path = fetchurl {
        name = "safe_buffer___safe_buffer_5.2.1.tgz";
        url  = "https://registry.yarnpkg.com/safe-buffer/-/safe-buffer-5.2.1.tgz";
        sha1 = "1eaf9fa9bdb1fdd4ec75f58f9cdb4e6b7827eec6";
      };
    }
    {
      name = "semver_diff___semver_diff_3.1.1.tgz";
      path = fetchurl {
        name = "semver_diff___semver_diff_3.1.1.tgz";
        url  = "https://registry.yarnpkg.com/semver-diff/-/semver-diff-3.1.1.tgz";
        sha1 = "05f77ce59f325e00e2706afd67bb506ddb1ca32b";
      };
    }
    {
      name = "semver___semver_5.7.1.tgz";
      path = fetchurl {
        name = "semver___semver_5.7.1.tgz";
        url  = "https://registry.yarnpkg.com/semver/-/semver-5.7.1.tgz";
        sha1 = "a954f931aeba508d307bbf069eff0c01c96116f7";
      };
    }
    {
      name = "semver___semver_6.3.0.tgz";
      path = fetchurl {
        name = "semver___semver_6.3.0.tgz";
        url  = "https://registry.yarnpkg.com/semver/-/semver-6.3.0.tgz";
        sha1 = "ee0a64c8af5e8ceea67687b133761e1becbd1d3d";
      };
    }
    {
      name = "signal_exit___signal_exit_3.0.4.tgz";
      path = fetchurl {
        name = "signal_exit___signal_exit_3.0.4.tgz";
        url  = "https://registry.yarnpkg.com/signal-exit/-/signal-exit-3.0.4.tgz";
        sha1 = "366a4684d175b9cab2081e3681fda3747b6c51d7";
      };
    }
    {
      name = "simple_swizzle___simple_swizzle_0.2.2.tgz";
      path = fetchurl {
        name = "simple_swizzle___simple_swizzle_0.2.2.tgz";
        url  = "https://registry.yarnpkg.com/simple-swizzle/-/simple-swizzle-0.2.2.tgz";
        sha1 = "a4da6b635ffcccca33f70d17cb92592de95e557a";
      };
    }
    {
      name = "source_map_js___source_map_js_0.6.2.tgz";
      path = fetchurl {
        name = "source_map_js___source_map_js_0.6.2.tgz";
        url  = "https://registry.yarnpkg.com/source-map-js/-/source-map-js-0.6.2.tgz";
        sha1 = "0bb5de631b41cfbda6cfba8bd05a80efdfd2385e";
      };
    }
    {
      name = "source_map___source_map_0.6.1.tgz";
      path = fetchurl {
        name = "source_map___source_map_0.6.1.tgz";
        url  = "https://registry.yarnpkg.com/source-map/-/source-map-0.6.1.tgz";
        sha1 = "74722af32e9614e9c287a8d0bbde48b5e2f1a263";
      };
    }
    {
      name = "sprintf_js___sprintf_js_1.0.3.tgz";
      path = fetchurl {
        name = "sprintf_js___sprintf_js_1.0.3.tgz";
        url  = "https://registry.yarnpkg.com/sprintf-js/-/sprintf-js-1.0.3.tgz";
        sha1 = "04e6926f662895354f3dd015203633b857297e2c";
      };
    }
    {
      name = "static_i18n___static_i18n_0.2.10.tgz";
      path = fetchurl {
        name = "static_i18n___static_i18n_0.2.10.tgz";
        url  = "https://registry.yarnpkg.com/static-i18n/-/static-i18n-0.2.10.tgz";
        sha1 = "2a934626260fa53c882226afed56131573dece11";
      };
    }
    {
      name = "string_width___string_width_3.1.0.tgz";
      path = fetchurl {
        name = "string_width___string_width_3.1.0.tgz";
        url  = "https://registry.yarnpkg.com/string-width/-/string-width-3.1.0.tgz";
        sha1 = "22767be21b62af1081574306f69ac51b62203961";
      };
    }
    {
      name = "string_width___string_width_4.2.2.tgz";
      path = fetchurl {
        name = "string_width___string_width_4.2.2.tgz";
        url  = "https://registry.yarnpkg.com/string-width/-/string-width-4.2.2.tgz";
        sha1 = "dafd4f9559a7585cfba529c6a0a4f73488ebd4c5";
      };
    }
    {
      name = "string_decoder___string_decoder_1.3.0.tgz";
      path = fetchurl {
        name = "string_decoder___string_decoder_1.3.0.tgz";
        url  = "https://registry.yarnpkg.com/string_decoder/-/string_decoder-1.3.0.tgz";
        sha1 = "42f114594a46cf1a8e30b0a84f56c78c3edac21e";
      };
    }
    {
      name = "strip_ansi___strip_ansi_5.2.0.tgz";
      path = fetchurl {
        name = "strip_ansi___strip_ansi_5.2.0.tgz";
        url  = "https://registry.yarnpkg.com/strip-ansi/-/strip-ansi-5.2.0.tgz";
        sha1 = "8c9a536feb6afc962bdfa5b104a5091c1ad9c0ae";
      };
    }
    {
      name = "strip_ansi___strip_ansi_6.0.1.tgz";
      path = fetchurl {
        name = "strip_ansi___strip_ansi_6.0.1.tgz";
        url  = "https://registry.yarnpkg.com/strip-ansi/-/strip-ansi-6.0.1.tgz";
        sha1 = "9e26c63d30f53443e9489495b2105d37b67a85d9";
      };
    }
    {
      name = "strip_json_comments___strip_json_comments_2.0.1.tgz";
      path = fetchurl {
        name = "strip_json_comments___strip_json_comments_2.0.1.tgz";
        url  = "https://registry.yarnpkg.com/strip-json-comments/-/strip-json-comments-2.0.1.tgz";
        sha1 = "3c531942e908c2697c0ec344858c286c7ca0a60a";
      };
    }
    {
      name = "supports_color___supports_color_5.4.0.tgz";
      path = fetchurl {
        name = "supports_color___supports_color_5.4.0.tgz";
        url  = "https://registry.yarnpkg.com/supports-color/-/supports-color-5.4.0.tgz";
        sha1 = "1c6b337402c2137605efe19f10fec390f6faab54";
      };
    }
    {
      name = "supports_color___supports_color_5.5.0.tgz";
      path = fetchurl {
        name = "supports_color___supports_color_5.5.0.tgz";
        url  = "https://registry.yarnpkg.com/supports-color/-/supports-color-5.5.0.tgz";
        sha1 = "e2e69a44ac8772f78a1ec0b35b689df6530efc8f";
      };
    }
    {
      name = "supports_color___supports_color_7.2.0.tgz";
      path = fetchurl {
        name = "supports_color___supports_color_7.2.0.tgz";
        url  = "https://registry.yarnpkg.com/supports-color/-/supports-color-7.2.0.tgz";
        sha1 = "1b7dcdcb32b8138801b3e478ba6a51caa89648da";
      };
    }
    {
      name = "tailwindcss___tailwindcss_2.1.1.tgz";
      path = fetchurl {
        name = "tailwindcss___tailwindcss_2.1.1.tgz";
        url  = "https://registry.yarnpkg.com/tailwindcss/-/tailwindcss-2.1.1.tgz";
        sha1 = "642f6038c9283a8e1454da34585b8b7c1a1e8877";
      };
    }
    {
      name = "term_size___term_size_2.2.1.tgz";
      path = fetchurl {
        name = "term_size___term_size_2.2.1.tgz";
        url  = "https://registry.yarnpkg.com/term-size/-/term-size-2.2.1.tgz";
        sha1 = "2a6a54840432c2fb6320fea0f415531e90189f54";
      };
    }
    {
      name = "to_readable_stream___to_readable_stream_1.0.0.tgz";
      path = fetchurl {
        name = "to_readable_stream___to_readable_stream_1.0.0.tgz";
        url  = "https://registry.yarnpkg.com/to-readable-stream/-/to-readable-stream-1.0.0.tgz";
        sha1 = "ce0aa0c2f3df6adf852efb404a783e77c0475771";
      };
    }
    {
      name = "to_regex_range___to_regex_range_5.0.1.tgz";
      path = fetchurl {
        name = "to_regex_range___to_regex_range_5.0.1.tgz";
        url  = "https://registry.yarnpkg.com/to-regex-range/-/to-regex-range-5.0.1.tgz";
        sha1 = "1648c44aae7c8d988a326018ed72f5b4dd0392e4";
      };
    }
    {
      name = "touch___touch_3.1.0.tgz";
      path = fetchurl {
        name = "touch___touch_3.1.0.tgz";
        url  = "https://registry.yarnpkg.com/touch/-/touch-3.1.0.tgz";
        sha1 = "fe365f5f75ec9ed4e56825e0bb76d24ab74af83b";
      };
    }
    {
      name = "type_fest___type_fest_0.8.1.tgz";
      path = fetchurl {
        name = "type_fest___type_fest_0.8.1.tgz";
        url  = "https://registry.yarnpkg.com/type-fest/-/type-fest-0.8.1.tgz";
        sha1 = "09e249ebde851d3b1e48d27c105444667f17b83d";
      };
    }
    {
      name = "typedarray_to_buffer___typedarray_to_buffer_3.1.5.tgz";
      path = fetchurl {
        name = "typedarray_to_buffer___typedarray_to_buffer_3.1.5.tgz";
        url  = "https://registry.yarnpkg.com/typedarray-to-buffer/-/typedarray-to-buffer-3.1.5.tgz";
        sha1 = "a97ee7a9ff42691b9f783ff1bc5112fe3fca9080";
      };
    }
    {
      name = "undefsafe___undefsafe_2.0.3.tgz";
      path = fetchurl {
        name = "undefsafe___undefsafe_2.0.3.tgz";
        url  = "https://registry.yarnpkg.com/undefsafe/-/undefsafe-2.0.3.tgz";
        sha1 = "6b166e7094ad46313b2202da7ecc2cd7cc6e7aae";
      };
    }
    {
      name = "unique_string___unique_string_2.0.0.tgz";
      path = fetchurl {
        name = "unique_string___unique_string_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/unique-string/-/unique-string-2.0.0.tgz";
        sha1 = "39c6451f81afb2749de2b233e3f7c5e8843bd89d";
      };
    }
    {
      name = "universalify___universalify_0.1.2.tgz";
      path = fetchurl {
        name = "universalify___universalify_0.1.2.tgz";
        url  = "https://registry.yarnpkg.com/universalify/-/universalify-0.1.2.tgz";
        sha1 = "b646f69be3942dabcecc9d6639c80dc105efaa66";
      };
    }
    {
      name = "universalify___universalify_2.0.0.tgz";
      path = fetchurl {
        name = "universalify___universalify_2.0.0.tgz";
        url  = "https://registry.yarnpkg.com/universalify/-/universalify-2.0.0.tgz";
        sha1 = "75a4984efedc4b08975c5aeb73f530d02df25717";
      };
    }
    {
      name = "update_notifier___update_notifier_4.1.3.tgz";
      path = fetchurl {
        name = "update_notifier___update_notifier_4.1.3.tgz";
        url  = "https://registry.yarnpkg.com/update-notifier/-/update-notifier-4.1.3.tgz";
        sha1 = "be86ee13e8ce48fb50043ff72057b5bd598e1ea3";
      };
    }
    {
      name = "url_parse_lax___url_parse_lax_3.0.0.tgz";
      path = fetchurl {
        name = "url_parse_lax___url_parse_lax_3.0.0.tgz";
        url  = "https://registry.yarnpkg.com/url-parse-lax/-/url-parse-lax-3.0.0.tgz";
        sha1 = "16b5cafc07dbe3676c1b1999177823d6503acb0c";
      };
    }
    {
      name = "util_deprecate___util_deprecate_1.0.2.tgz";
      path = fetchurl {
        name = "util_deprecate___util_deprecate_1.0.2.tgz";
        url  = "https://registry.yarnpkg.com/util-deprecate/-/util-deprecate-1.0.2.tgz";
        sha1 = "450d4dc9fa70de732762fbd2d4a28981419a0ccf";
      };
    }
    {
      name = "widest_line___widest_line_3.1.0.tgz";
      path = fetchurl {
        name = "widest_line___widest_line_3.1.0.tgz";
        url  = "https://registry.yarnpkg.com/widest-line/-/widest-line-3.1.0.tgz";
        sha1 = "8292333bbf66cb45ff0de1603b136b7ae1496eca";
      };
    }
    {
      name = "wrappy___wrappy_1.0.2.tgz";
      path = fetchurl {
        name = "wrappy___wrappy_1.0.2.tgz";
        url  = "https://registry.yarnpkg.com/wrappy/-/wrappy-1.0.2.tgz";
        sha1 = "b5243d8f3ec1aa35f1364605bc0d1036e30ab69f";
      };
    }
    {
      name = "write_file_atomic___write_file_atomic_3.0.3.tgz";
      path = fetchurl {
        name = "write_file_atomic___write_file_atomic_3.0.3.tgz";
        url  = "https://registry.yarnpkg.com/write-file-atomic/-/write-file-atomic-3.0.3.tgz";
        sha1 = "56bd5c5a5c70481cd19c571bd39ab965a5de56e8";
      };
    }
    {
      name = "xdg_basedir___xdg_basedir_4.0.0.tgz";
      path = fetchurl {
        name = "xdg_basedir___xdg_basedir_4.0.0.tgz";
        url  = "https://registry.yarnpkg.com/xdg-basedir/-/xdg-basedir-4.0.0.tgz";
        sha1 = "4bc8d9984403696225ef83a1573cbbcb4e79db13";
      };
    }
    {
      name = "xtend___xtend_4.0.2.tgz";
      path = fetchurl {
        name = "xtend___xtend_4.0.2.tgz";
        url  = "https://registry.yarnpkg.com/xtend/-/xtend-4.0.2.tgz";
        sha1 = "bb72779f5fa465186b1f438f674fa347fdb5db54";
      };
    }
  ];
}
