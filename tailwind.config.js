module.exports = {
    purge: ['./public/**/*.html'],
    darkMode: false, // or 'media' or 'class'
    theme: {
      extend: {
        // ref: https://animista.net/play/basic
        animation: {
          "slide-in-bck-center":
            "slide-in-bck-center 10.0s cubic-bezier(0.250, 0.460, 0.450, 0.940) both",
        },
        keyframes: {
          "slide-in-bck-center": {
            "0%": { transform: "translateZ(600px)", opacity: "0" },
            to: { transform: "translateZ(0)", opacity: "1" }
          },
        },        
        fontFamily: {
          sans: 'Courier Prime, monospace',
        },
      },
    },
    variants: ['responsive', 'hover', 'focus'],
    plugins: [],
  };
  